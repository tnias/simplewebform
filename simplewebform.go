package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
)

var registerTmp *template.Template

type Server struct {
	sync.Mutex
	users *os.File
}

func cleanStr(in string) string {
	in = strings.Replace(in, "'", "", -1)
	in = strings.Replace(in, "\n", "", -1)
	return in
}

func (s *Server) myform(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		err := registerTmp.Execute(w, nil)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		io.WriteString(w, "processing data..\n")
		r.ParseForm()
		s.Lock()
		defer s.Unlock()

		for k, v := range r.Form {
			fmt.Fprintf(s.users, "'%v': '%v', ",
				cleanStr(k), cleanStr(strings.Join(v, " ")))
		}
		fmt.Fprintf(s.users, "\n")

		io.WriteString(w, "done.")
	}
}

func main() {
	// load template
	t, err := template.ParseFiles("register.html")
	if err != nil {
		log.Fatal(err)
	}
	registerTmp = t

	// open user file
	uf, err := os.OpenFile("users.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		log.Fatal(err)
	}

	// register handlers
	s := &Server{users: uf}
	http.HandleFunc("/", s.myform)

	// start
	http.ListenAndServe(":8000", nil)
}
