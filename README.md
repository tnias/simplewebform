A minimalistic register form without input validation. It just dumps all data
to an easily parsable file.

For security reasons `'` and `\n` characters in the user supplied data will be
removed before data gets writing to file.
